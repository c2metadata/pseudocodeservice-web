### Quick Reference
#### Pseudocode Service endpoints:
This API is used to translate SDTL JSON into humman readable text. 

It only has one endpoint, which can be accessed by attaching the following path after the ip:port.

/pseudocodeservice-web/pseudocode/view

You have to attach a file containing the SDTL in order for the API to translate it.
